import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllConstants } from 'src/app/util/AllConstants';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  private url = `${AllConstants.BASE_URL}/api/subjects`;

  constructor(private http: HttpClient) { }

  getAllSubjects() {
    return this.http.get<any>(this.url);
  }

  getSubscribedSubjects() {
    return this.http.get<any>(`${this.url}/subscribed`);
  }

  subscribeToSubject(subject) {
    return this.http.post<any>(`${this.url}/subscribe`, subject);
  }

  unsubscribeFromSubject(subject) {
    return this.http.post<any>(`${this.url}/unsubscribe`, subject);
  }
}
