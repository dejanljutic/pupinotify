import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    let cloned;
    let headers;
    
    if (token) {
      headers = new HttpHeaders({
        'x-time-zone': timeZone,
        'x-auth-token': token
      });
    } else {
      headers = new HttpHeaders({
        'x-time-zone': timeZone,
      });
    }

    cloned = req.clone({headers});
    
    return next.handle(cloned);
  }
}
