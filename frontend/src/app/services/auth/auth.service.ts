import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AllConstants } from 'src/app/util/AllConstants';
import { User } from 'src/app/models/User';
import { Token } from 'src/app/models/Token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = `${AllConstants.BASE_URL}/api/auth`;

  constructor(private http: HttpClient) { }

  login(user) {
    return this.http.post<any>(this.url, user);
  }

  logout() {
    localStorage.removeItem('token');
  }

  isAuthenticated(): boolean {
    if (localStorage.getItem('token')) {
      return true;
    }

    return false;
  }
}
