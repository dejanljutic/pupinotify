import { Injectable } from '@angular/core';
import { AllConstants } from '../../util/AllConstants';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = `${AllConstants.BASE_URL}/api/users`;

  constructor(private http: HttpClient) { }

  register(user: User) {
    return this.http.post<any>(this.url, user);
  }

  getUser() {
    return this.http.get<User>(this.url);
  }
}
