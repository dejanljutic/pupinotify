import { AbstractControl } from "@angular/forms";

export function MatchPasswords(control: AbstractControl) {
  if (control.parent === undefined) 
    return null;

  const password = control.parent.controls['password'].value;
  const confirmPassword = control.parent.controls['confirmPassword'].value;

  if (password !== confirmPassword) {
    return { MatchPasswords: true }
  } else {
    return null;
  }
}