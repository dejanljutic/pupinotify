import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { User } from '../models/User';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { SubjectService } from '../services/subject/subject.service';

@Injectable({
  providedIn: 'root'
})
export class SubjectResolverService implements Resolve<User> {

  constructor(private subjectService: SubjectService, private router: Router) { }

  resolve(): Observable<User> {
    return this.subjectService.getAllSubjects().pipe(
      map((subject) => {
        if (subject) {
          return subject;
        }

        return null;
      }),
      catchError(err => {
        this.router.navigateByUrl('/not-found');
        return of(null);
      })
    );
  }
}
