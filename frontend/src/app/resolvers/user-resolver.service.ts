import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { User } from '../models/User';
import { UserService } from '../services/user/user.service';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {

  constructor(private userService: UserService, private router: Router) { }

  resolve(): Observable<User> {
    return this.userService.getUser().pipe(
      map((user) => {
        if (user) {
          return user;
        }

        return null;
      }),
      catchError(err => {
        this.router.navigateByUrl('/not-found');
        return of(null);
      })
    );
  }
}
