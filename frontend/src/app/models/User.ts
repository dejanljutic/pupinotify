export class User {
    email: string = '';
    password: string = '';
    username: string = '';
    subscriptions: Array<string> = [];
}