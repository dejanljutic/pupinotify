export class Subject {
    id: string;
    name: string;
    subscribers: Array<string> = [];
}