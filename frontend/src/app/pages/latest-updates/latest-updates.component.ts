import { Component, OnInit } from '@angular/core';
import { SubjectService } from 'src/app/services/subject/subject.service';
import * as moment from 'moment';

@Component({
  selector: 'app-latest-updates',
  templateUrl: './latest-updates.component.html',
  styleUrls: ['./latest-updates.component.css']
})
export class LatestUpdatesComponent implements OnInit {
  public subjects = [];
  public moment = moment;

  constructor(private subjectService: SubjectService) { }

  ngOnInit() {
    this.getSubscribedSubjects();
  }

  getSubscribedSubjects() {
    this.subjectService.getSubscribedSubjects().subscribe(
      (result) => {
        this.subjects = result;
      },
      (err) => {
        console.log(err);
      }
    )
  }
  
}
