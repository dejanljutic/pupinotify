import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/User';
import { SubjectService } from 'src/app/services/subject/subject.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  loggedUser: User;
  subscribedSubjects: [];

  constructor(private route: ActivatedRoute, private subjectService: SubjectService) { }

  ngOnInit() {
    this.loggedUser = this.route.snapshot.data.user;
    this.getSubscribedSubjects();
  }

  getSubscribedSubjects() {
    this.subjectService.getSubscribedSubjects().
    subscribe(
      (result) => {
        this.subscribedSubjects = result;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  unsubscribeFromSubject(subject) {
    this.subjectService.unsubscribeFromSubject(subject).
    subscribe(
      (result) => {
        this.getSubscribedSubjects();
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
