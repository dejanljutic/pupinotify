import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatchPasswords } from 'src/app/validators/password-match-validator';
import { UserService } from 'src/app/services/user/user.service';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  public registrationForm: FormGroup;
  public registrationError: String;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.registrationForm = this.createFormGroup();
  }

  createFormGroup() {
    return new FormGroup({
      username: new FormControl('', Validators.compose([
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9]{8}/)
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required,
        MatchPasswords
      ]))
    });
  }

  goToLogin() {
    this.router.navigateByUrl('/login');
  }

  register() {
    const user = _.omit(this.registrationForm.value, ['confirmPassword']);
    console.log(user);
    this.userService.register(user).
      subscribe(
        (result) => {
          localStorage.setItem('token', result.token);
          this.router.navigateByUrl('/profile');
        },
        (err) => {
          this.registrationError = err.error;
        }
      );
  }

  get email() {
    return this.registrationForm.get('email');
  }
  get password() {
    return this.registrationForm.get('password');
  }
  get username() {
    return this.registrationForm.get('username');
  }
  get confirmPassword() {
    return this.registrationForm.get('confirmPassword');
  }
}
