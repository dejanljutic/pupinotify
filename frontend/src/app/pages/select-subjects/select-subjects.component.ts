import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubjectService } from 'src/app/services/subject/subject.service';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select-subjects',
  templateUrl: './select-subjects.component.html',
  styleUrls: ['./select-subjects.component.css']
})
export class SelectSubjectsComponent implements OnInit {
  subjects = [];
  selectedSubject: String;
  @Output() newSubscriptionEvent = new EventEmitter();

  constructor(private route: ActivatedRoute, private subjectService: SubjectService) { }

  ngOnInit() {
    this.subjects = this.route.snapshot.data.subjects;
  }

  subscribeToSubject() {
    this.subjectService.subscribeToSubject({ id: this.selectedSubject }).
    subscribe(
      (result) => {
        this.selectedSubject = undefined;
        this.newSubscriptionEvent.emit();
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
