import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public loginError: String;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.createFormGroup();
  }

  createFormGroup() {
    return new FormGroup({ 
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required
      ])),
    });
  }

  login() {
    const user = this.loginForm.value;
    this.authService.login(user).
      subscribe(
        (result) => {
          localStorage.setItem('token', result.token);
          this.router.navigateByUrl('/latest-updates');
        },
        (err) => {
          this.loginError = err.error;
        }
      )
  }

  goToRegister() {
    this.router.navigateByUrl('/register');
  }

  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }

}
