import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { UserResolverService } from './resolvers/user-resolver.service';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { SubjectResolverService } from './resolvers/subject-resolver.service';
import { SelectSubjectsComponent } from './pages/select-subjects/select-subjects.component';
import { LoginComponent } from './pages/login/login.component';
import { LatestUpdatesComponent } from './pages/latest-updates/latest-updates.component';
import { AuthGuard } from './guards/auth.guard';

let appRoutes: Routes = [
  { path: '', component: HomeComponent, 
  children: [
    { path: 'latest-updates', component: LatestUpdatesComponent, canActivate: [AuthGuard], resolve: { user: UserResolverService } },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrationComponent },
  ] },
  { path: 'select-subjects', component: SelectSubjectsComponent, resolve: { subjects: SubjectResolverService } },
  { path: 'about', component: AboutUsComponent },
  { path: 'profile', component: UserProfileComponent, resolve: { user: UserResolverService, subjects: SubjectResolverService } },
  { path: 'not-found', component: ErrorPageComponent },
  { path: '**', redirectTo: '/not-found' }
];

export let routes = appRoutes;
