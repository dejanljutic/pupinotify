# Pupinotify

Pupinotify is the codename for a learning project which could potentially help a large number of students at the Technical Faculty "Mihajlo Pupin" Zrenjanin.

## Problem

As the faculty [website](http://www.tfzr.uns.ac.rs/) is quite old and isn't very user friendly, a third-party app could fill the gaps and vastly improve the user experience.

The problem I'm trying to solve, is that every subject has a dedicated page to which the professors and teaching assistants can post notifications about the subject, results of exams, materials for preparing exams, etc.

For example, after an exam, we students usually wait anywhere between a few days and 2 - 3  weeks before seeing the results on the subject page. When the results will be ready - no one knows. That means that students must regularly check the website in order to see if the results are ready.

## Solution

Pupinotify abstracts the website checking part, and notifies students only when a new post is added.

## Usage

Students are required to create an account with a valid email address and select the subjects which they wish to receive updates from. Pupinotify does the rest.

## Architecture

The app is developed using the MEAN stack.
