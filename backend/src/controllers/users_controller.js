const bcrypt = require("bcryptjs");
const { User, validateUser } = require("../models/user_model");

exports.create = async (req, res) => {
    try {
        const { error } = validateUser(req.body);
        if (error)
            return res.status(400).send(error);

        if (await User.findOne({ username: req.body.username }))
            return res.status(400).send('Uneto korisničko ime već postoji.');

        if (await User.findOne({ email: req.body.email }))
            return res.status(400).send('Uneta e-mail adresa već postoji.');

        const user = new User(req.body);
        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(user.password, salt);

        await user.save();

        console.log('Registration block - User registered: ' + user.email);

        const token = user.generateAccessToken();

        return res.status(201).send({token});
    } catch (ex) {
        console.log(ex);
        return res.status(500).send(ex.message);
    }
};

exports.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.user._id);

    if (!user) {
      return res.status(404).send();
    }

    res.send(user);
  } catch(ex) {
    res.status(500).send();
  }
}