const puppeteer = require('puppeteer');
const mongoose = require("mongoose");
const moment = require('moment'); 
const { Subject } = require("../models/subject_model");
const { sendMailToSubscribers } = require("../utils/mail");

// nadji h2 svih postova
exports.scrapeAllPosts = async () => {
    try {
        const subjects = await Subject.find({ subscribers: { $exists: true, $ne: [] } });
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.setRequestInterception(true);
        page.on('request', request => {
            if (['image', 'stylesheet', 'font', 'script'].indexOf(request.resourceType()) !== -1)
                request.abort();
            else
                request.continue();
        });

        for (subject of subjects) {
            let edited = false;
            let title = '';
            let url = '';
            console.log(subject.name);
            for (section of subject.sections) {
                try {
                    await page.goto(section.url);
                    const element = await page.$('.post > h2');
                    if (element) {
                        const post = await element.getProperty('textContent');
                        const postText = await post.jsonValue();
    
                        if (section.lastPost !== postText) {
                            console.log('found a new post!');
                            section.lastPost = postText ? postText : ""; 
                            title = postText ? postText : "";
                            url = section.url;
                            edited = true;
                        }
                    }
                } catch (error) {
                    console.log(error);
                }
            }
            if (edited) {
                subject.lastUpdateTitle = title;
                subject.lastUpdateUrl = url;
                subject.lastUpdateTime = moment().format();
                await subject.save();
                await sendMailToSubscribers(subject);
            }
        }
        console.log('all posts scraped');

        await browser.close();

        return;
    } catch (error) {
        console.log(error);
    }
}

// add section names and urls to existing subjects in db
exports.scrapeAllSections = async () => {
    const subjects = await Subject.find();
    console.log('started scraping');
    for (subject of subjects) {
        await scrapeSectionLinks(subject._id);
        console.log(`added sections for ${subject.name}`);
    }
}

const scrapeSectionLinks = async (_id) => {
    try {
        const subject = await Subject.findById(_id);
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        subject.updateOne({ $set: { sections: [] }}, function(err, affected){
            if (err) 
                console.log(err);
        });

        await page.setRequestInterception(true);
        page.on('request', request => {
            if (['image', 'stylesheet', 'font', 'script'].indexOf(request.resourceType()) !== -1)
                request.abort();
            else
                request.continue();
        });

        await page.goto(subject.url);

        const menu = await page.$$('.sidemenu > ul > li > a');

        for (const item of menu) {
            const section = await item.getProperty('href');
            const title = await item.getProperty('textContent');
            const sectionLink = await section.jsonValue();
            const sectionName = await title.jsonValue();

            subject.sections.push({ name: sectionName, url: sectionLink });
        }

        await subject.save();
        await browser.close();

        return 1;
    } catch (error) {
        console.log(error);
    }
}

// add all subjects to the db
exports.scrapeAllSubjects = async () => {
    try {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.setRequestInterception(true);
        page.on('request', request => {
            if (['image', 'stylesheet', 'font', 'script'].indexOf(request.resourceType()) !== -1)
                request.abort();
            else
                request.continue();
        });

        await page.goto('http://www.tfzr.uns.ac.rs/Predmeti');

        const elementHandle = await page.$x('//*[@id="txtFilter"]');
        await elementHandle[0].type(String.fromCharCode(13));

        const subjectList = await page.$$('#rezultati > p > a');

        const subjects = [];

        for (subject of subjectList) {
            const subjectName = await subject.getProperty('textContent');
            const subjectUrl = await subject.getProperty('href');
            const name = await subjectName.jsonValue();
            const url = await subjectUrl.jsonValue();
            subjects.push({ name, url });
        }

        await browser.close();

        for (subject of subjects) {
            if (subject.name && subject.url) {
                const newSubject = new Subject({ ...subject });
                await newSubject.save();
            }
        }

        return;
    } catch (error) {
        console.log(error);
    }
}