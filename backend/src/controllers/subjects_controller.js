const { Subject } = require("../models/subject_model");
const { User } = require("../models/user_model");

exports.getAllSubjects = async (req, res) => {
    try {
        const subjects = await Subject.find().select('_id name');

        if (!subjects) {
        return res.status(404).send();
        }

        res.status(200).send(subjects);
    } catch(ex) {
        res.status(500).send();
    }   
}

exports.getSubscribedSubjects = async (req, res) => {
    try {
        const user = await User.findById(req.user._id);
        const subjects = [];

        for (id of user.subscriptions) {
            const subject = await Subject.findById(id);
            subjects.push(subject);
        }

        subjects.reverse();

        res.status(200).send(subjects);
    } catch(ex) {
        res.status(500).send();
    }   
}

exports.subscribeToSubject = async (req, res) => {
    try {
        const user = await User.findById(req.user._id);
        const subject = await Subject.findById(req.body.id);

        user.subscriptions.push(subject._id);
        subject.subscribers.push(user._id);
        await user.save();
        await subject.save();

        res.status(200).send();
    } catch(ex) {
        res.status(500).send();
    }   
}

exports.unsubscribeFromSubject = async (req, res) => {
    try {
        const user = await User.findById(req.user._id);
        const subject = await Subject.findById(req.body._id);

        const userIndex = user.subscriptions.indexOf(subject._id);
        if (userIndex > -1) 
            user.subscriptions.splice(userIndex, 1);

        const subjectIndex = subject.subscribers.indexOf(user._id);
        if (subjectIndex > -1) 
            subject.subscribers.splice(subjectIndex, 1);
        
        await user.save();
        await subject.save();

        res.status(200).send();
    } catch(ex) {
        res.status(500).send();
    }   
}