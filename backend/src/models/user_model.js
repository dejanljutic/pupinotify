const Joi = require("@hapi/joi");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const config = require("config");

const schema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        minlength: 3,
        maxlength: 50
    },
    email: {
        type: String,
        unique: true,
        required: true,
        minlength: 4,
        maxlength: 225
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 1024
    },
    subscriptions: {
        type: Array
    }
});

schema.methods.generateAccessToken = function () {
    const accessToken = jwt.sign({ _id: this._id }, config.get('jwtPrivateKey'));

    return accessToken;
};

const User = mongoose.model('User', schema);

const validateUser = (user) => {
    const schema = Joi.object({
        username: Joi.string().min(2).max(225),
        email: Joi.string().min(4).max(225).required().email(),
        password: Joi.string().regex(/[a-zA-Z0-9]{8}/),
    });

    return schema.validate(user);
}

exports.User = User;
exports.validateUser = validateUser;