const mongoose = require("mongoose");

const sectionSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    lastPost: {
        type: String
    }
});

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    sections: {
        type: [sectionSchema]
    },
    subscribers: {
        type: Array
    },
    lastUpdateTitle: {
        type: String
    },
    lastUpdateUrl: {
        type: String
    },
    lastUpdateTime: {
        type: String
    }
});

const Subject = mongoose.model('Subject', schema);

exports.Subject = Subject;