const nodemailer = require('nodemailer');
const config = require("config");
const { User } = require("../models/user_model");

let transporter = nodemailer.createTransport({
    host: "smtp.mailgun.org",
    port: 587,
    secure: false,
    auth: {
        user: config.get('mail_user'),
        pass: config.get('mail_pass'),
    },
  });

const sendMail = (options) => {
    const message = `
    <p>Ćao, ${options.username}</p>
    <p>Objavljeno je novo obaveštenje na stranici predmeta ${options.subject}, možeš ga pronaći na sledećem linku: </p>
    <a href="${options.url}" style="color: #17a2b8;" target="_blank" rel="noopener noreferrer">${options.update}<a/>
    <br>`;
    const footer = `
    <p>S poštovanjem,
    <br>
    Pupinotify</p>`;

    const mailOptions = {
      from: 'pupinotify@pupinotify.rs',
      to: options.email,
      subject: `${options.subject} - novo obaveštenje`,
      html: message + footer
    };
  
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) console.log(err)
    });
}

exports.sendMailToSubscribers = async (subject) => {
  for (studentId of subject.subscribers) {
      const student = await User.findById(studentId);
      if (student) {
        const options = {
          username: student.username,
          email: student.email,
          subject: subject.name,
          url: subject.lastUpdateUrl,
          update: subject.lastUpdateTitle
        };

        sendMail(options);
      }
  }
}