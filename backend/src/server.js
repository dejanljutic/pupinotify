const app = require('express')();
const cron = require('node-cron');
const http = require('http').Server(app);
const { scrapeAllSubjects, scrapeAllSections, scrapeAllPosts } = require('./controllers/scrape_controller');

// Connect mongodb
require("./startup/db")();

// Load routes
require("./startup/routes")(app);
require("./startup/config")();

// cron.schedule('0 0 1 10 *', function () { // jednom godisnje
//     scrapeAllSubjects();
// }); // this method pulls all subjects into the db

// cron.schedule('0 0 * * 0', function () { // jednom nedeljno
//     scrapeAllSections();
// }); // this method saves all sections for every subject

cron.schedule('*/5 * * * *', function () { // na svakih 5 minuta
    scrapeAllPosts();
}); // this method goes through every section and checks for new posts

// Start express server
const port = process.env.PORT || 3000;
http.listen(port, () => console.log(`Listening on port ${port}...`));