const express = require("express");
const cors = require('cors');
const auth = require('../routes/auth_route');
const users = require('../routes/users_route');
const subjects = require('../routes/subjects_route');

module.exports = function(app) {
  // Parse incoming request bodies middleware
  app.use(cors({
    origin: '*'
  }));
  app.use(express.json());

  app.use('/api/auth', auth);
  app.use('/api/users', users);
  app.use('/api/subjects', subjects);
};