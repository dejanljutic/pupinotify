const express = require("express");
const router = express.Router();
const auth = require("../middlewares/authMiddleware");
const subjectController = require("../controllers/subjects_controller");

router.get('/', auth, subjectController.getAllSubjects);
router.get('/subscribed', auth, subjectController.getSubscribedSubjects);
router.post('/subscribe', auth, subjectController.subscribeToSubject);
router.post('/unsubscribe', auth, subjectController.unsubscribeFromSubject);

module.exports = router;